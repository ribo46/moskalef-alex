//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit4.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm4 *Form4;
//---------------------------------------------------------------------------
__fastcall TForm4::TForm4(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm4::Button1Click(TObject *Sender)
{
	OpenDialog1->Filter = TMediaCodecManager::GetFilterString();
	if (OpenDialog1->Execute()) {
		MediaPlayer1->FileName =  OpenDialog1->FileName;
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm4::TrackBar1Change(TObject *Sender)
{
	TetheringAppProfile1->Resources->FindByName("Volume")->Value = TrackBar1->Value;
	TetheringAppProfile1->Resources->FindByName("VolumeMax")->Value = TrackBar1->Max;
}
//---------------------------------------------------------------------------

void __fastcall TForm4::TrackBar2Change(TObject *Sender)
{
	TetheringAppProfile1->Resources->FindByName("Time")->Value = TrackBar2->Value;
    TetheringAppProfile1->Resources->FindByName("TimeMax")->Value = TrackBar2->Value;
}
//---------------------------------------------------------------------------

void __fastcall TForm4::TetheringAppProfile1ResourceReceived(TObject * const Sender,
          TRemoteResource * const AResource)
{
    Memo1->Lines->Add(AResource->Hint + " = " + AResource->Value.AsString);
}
//---------------------------------------------------------------------------

