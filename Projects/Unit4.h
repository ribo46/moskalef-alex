//---------------------------------------------------------------------------

#ifndef Unit4H
#define Unit4H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.Media.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class TForm4 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TLayout *Layout1;
	TLabel *Label1;
	TMediaPlayer *MediaPlayer1;
	TMediaPlayerControl *MediaPlayerControl1;
	TActionList *ActionList1;
	TOpenDialog *OpenDialog1;
	TTetheringManager *TetheringManager1;
	TTetheringAppProfile *TetheringAppProfile1;
	TLayout *Layout2;
	TLabel *Label2;
	TLabel *Label3;
	TMemo *Memo1;
	TAction *Action1;
	TMediaPlayerPlayPause *MediaPlayerPlayPause;
	TMediaPlayerStop *MediaPlayerStop;
	TMediaPlayerCurrentTime *MediaPlayerCurrentTime1;
	TMediaPlayerVolume *MediaPlayerVolume1;
	TAction *asClear;
	TAction *asPlayPause;
	TAction *asStop;
	TTrackBar *TrackBar1;
	TTrackBar *TrackBar2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall TrackBar2Change(TObject *Sender);
	void __fastcall TetheringAppProfile1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);



private:	// User declarations
public:		// User declarations
	__fastcall TForm4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm4 *Form4;
//---------------------------------------------------------------------------
#endif
