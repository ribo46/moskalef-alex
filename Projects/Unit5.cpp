//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit5.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm5 *Form5;
//---------------------------------------------------------------------------
__fastcall TForm5::TForm5(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm5::FormShow(TObject *Sender)
{
	TetheringManager1->DiscoverManagers();
}
//---------------------------------------------------------------------------

void __fastcall TForm5::Button1Click(TObject *Sender)
{
   TetheringManager1->DiscoverManagers();
}
//---------------------------------------------------------------------------

void __fastcall TForm5::TetheringManager1EndProfilesDiscovery(TObject * const Sender,
		  TTetheringProfileInfoList * const ARemoteProfiles)
{
	ListBox1->Clear();
	for (int i = 0; i < TetheringManager1->RemoteProfiles->Count; i++) {
	   ListBox1->Items->Add(TetheringManager1->RemoteProfiles->Items[i].ProfileText);	
	}
	if (ListBox1->Count > 0) {
		ListBox1->ItemIndex = 0;
		TetheringAppProfile1->Connect(TetheringManager1->RemoteProfiles->Items[ListBox1->ItemIndex]);
	}
}
//---------------------------------------------------------------------------

