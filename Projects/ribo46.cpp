//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "ribo46.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	edName->Text = "Student" + IntToStr(Random(99));
	edName123->Text = "Privet kak dela?";
	ttm->AutoConnect();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	for(int i = 0; i < ttp->ConnectedProfiles->Count; i++) {
		ttp->SendString ( ttp->ConnectedProfiles->Items[i], edName->Text, edName123->Text);
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
	me->Lines->Add(AResource->Hint +": " + AResource->Value.AsString);
}
//---------------------------------------------------------------------------
