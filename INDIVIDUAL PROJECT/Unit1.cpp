//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include <math.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------

 	int a = 0;

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::backClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button2Click(TObject *Sender)
{
    ShowMessage("Moskalef 151-362");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1Click(TObject *Sender)
{
	tc->ActiveTab = tiSun;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::buFactsClick(TObject *Sender)
{
	if (a==0)
	{
		Rectangle1->Opacity=1;
		FloatAnimation2->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle1->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFactsMercClick(TObject *Sender)
{
	if (a==0)
	{
		Rectangle2->Opacity=1;
		FloatAnimation3->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle2->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image2Click(TObject *Sender)
{
	tc->ActiveTab=tiMerc;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFactVeneraClick(TObject *Sender)
{
		if (a==0)
	{
		Rectangle3->Opacity=1;
		FloatAnimation5->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle3->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFactsEarthClick(TObject *Sender)
{
		if (a==0)
	{
		Rectangle4->Opacity=1;
		FloatAnimation7->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle4->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image3Click(TObject *Sender)
{
	tc->ActiveTab=tiVenera;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image4Click(TObject *Sender)
{
	tc->ActiveTab=tiEarth;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFactsMarsClick(TObject *Sender)
{
			if (a==0)
	{
		Rectangle5->Opacity=1;
		FloatAnimation10->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle5->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image5Click(TObject *Sender)
{
	tc->ActiveTab=tiMars;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buFactUpiterClick(TObject *Sender)
{
			if (a==0)
	{
		Rectangle6->Opacity=1;
		FloatAnimation12->Enabled = true;
		a=1;
	}
	else if (a==1) {
		Rectangle6->Opacity=0;
		a=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image6Click(TObject *Sender)
{
    tc->ActiveTab=tiUpiter;
}
//---------------------------------------------------------------------------

