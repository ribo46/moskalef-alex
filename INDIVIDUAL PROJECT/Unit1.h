//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Layers3D.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Types3D.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiSun;
	TTabItem *tiMerc;
	TTabItem *tiVenera;
	TDummy *Dummy1;
	TSphere *Sphere1;
	TViewport3D *Viewport3D1;
	TFloatAnimation *FloatAnimation1;
	TLight *Light1;
	TImage3D *Image3D1;
	TLightMaterialSource *LightMaterialSource1;
	TText *Text1;
	TButton *back;
	TRectangle *Rectangle1;
	TLabel *Label1;
	TButton *buFacts;
	TFloatAnimation *FloatAnimation2;
	TToolBar *ToolBar1;
	TLabel *Label2;
	TButton *Button2;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TImage *Image7;
	TImage *Image8;
	TImage *Image9;
	TToolBar *ToolBar2;
	TLabel *Label3;
	TRectangle *Rectangle2;
	TFloatAnimation *FloatAnimation3;
	TLabel *Label4;
	TToolBar *ToolBar3;
	TLabel *Label5;
	TButton *buFactsMerc;
	TButton *Button3;
	TViewport3D *Viewport3D2;
	TDummy *Dummy2;
	TSphere *Sphere2;
	TFloatAnimation *FloatAnimation4;
	TLight *Light2;
	TImage3D *Image3D2;
	TText *Text2;
	TLightMaterialSource *LightMaterialSource2;
	TRectangle *Rectangle3;
	TFloatAnimation *FloatAnimation5;
	TLabel *Label6;
	TToolBar *ToolBar4;
	TLabel *Label7;
	TButton *buFactVenera;
	TButton *Button4;
	TViewport3D *Viewport3D3;
	TDummy *Dummy3;
	TSphere *Sphere3;
	TFloatAnimation *FloatAnimation6;
	TLight *Light3;
	TImage3D *Image3D3;
	TText *Text3;
	TLightMaterialSource *LightMaterialSource3;
	TTabItem *tiEarth;
	TRectangle *Rectangle4;
	TFloatAnimation *FloatAnimation7;
	TLabel *Label8;
	TToolBar *ToolBar5;
	TLabel *Label9;
	TButton *buFactsEarth;
	TButton *Button5;
	TViewport3D *Viewport3D4;
	TDummy *Dummy4;
	TSphere *Sphere4;
	TFloatAnimation *FloatAnimation8;
	TLight *Light4;
	TImage3D *Image3D4;
	TText *Text4;
	TLightMaterialSource *LightMaterialSource4;
	TSphere *Sphere5;
	TFloatAnimation *FloatAnimation9;
	TLightMaterialSource *LightMaterialSource5;
	TTabItem *tiMars;
	TRectangle *Rectangle5;
	TFloatAnimation *FloatAnimation10;
	TLabel *Label10;
	TToolBar *ToolBar6;
	TLabel *Label11;
	TButton *buFactsMars;
	TButton *Button6;
	TViewport3D *Viewport3D5;
	TDummy *Dummy5;
	TSphere *Sphere6;
	TFloatAnimation *FloatAnimation11;
	TLight *Light5;
	TImage3D *Image3D5;
	TText *Text5;
	TLightMaterialSource *LightMaterialSource6;
	TTabItem *tiUpiter;
	TRectangle *Rectangle6;
	TFloatAnimation *FloatAnimation12;
	TLabel *Label12;
	TToolBar *ToolBar7;
	TLabel *Label13;
	TButton *buFactUpiter;
	TButton *Button7;
	TViewport3D *Viewport3D6;
	TDummy *Dummy6;
	TSphere *Sphere7;
	TFloatAnimation *FloatAnimation13;
	TLight *Light6;
	TImage3D *Image3D6;
	TText *Text6;
	TLightMaterialSource *LightMaterialSource7;
	void __fastcall backClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall buFactsClick(TObject *Sender);
	void __fastcall buFactsMercClick(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
	void __fastcall buFactVeneraClick(TObject *Sender);
	void __fastcall buFactsEarthClick(TObject *Sender);
	void __fastcall Image3Click(TObject *Sender);
	void __fastcall Image4Click(TObject *Sender);
	void __fastcall buFactsMarsClick(TObject *Sender);
	void __fastcall Image5Click(TObject *Sender);
	void __fastcall buFactUpiterClick(TObject *Sender);
	void __fastcall Image6Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
