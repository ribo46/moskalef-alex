//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	laName->Text = dm->cdsFilmsNAME->Value;
	laDate->Text = dm->cdsFilmsDATE->Value;
	laGenre->Text = dm->cdsFilmsGENRE->Value;
	laDescription->Text = dm->cdsFilmsDESCRIPTION->Value;
	imFilm->Bitmap->Assign(dm->cdsFilmsIMG);
	tc->GotoVisibleTab(tiMore->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    tc->ActiveTab = tiRandom;
}
//---------------------------------------------------------------------------




void __fastcall Tfm::Button3Click(TObject *Sender)
{
    ShowMessage("Москалев Александр 151-362");
}
//---------------------------------------------------------------------------

