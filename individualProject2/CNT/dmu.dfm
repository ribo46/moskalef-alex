object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmAAA'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 80
    Top = 104
  end
  object cdsFilms: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFilms'
    RemoteServer = DSProviderConnection1
    Left = 64
    Top = 184
    object cdsFilmsID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsFilmsNAME: TWideStringField
      FieldName = 'NAME'
      Size = 400
    end
    object cdsFilmsIMG: TBlobField
      FieldName = 'IMG'
      Size = 1
    end
    object cdsFilmsGENRE: TWideStringField
      FieldName = 'GENRE'
      Size = 120
    end
    object cdsFilmsDATE: TDateField
      FieldName = 'DATE'
    end
    object cdsFilmsDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 2008
    end
  end
end
