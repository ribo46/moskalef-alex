//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiList;
	TButton *Button1;
	TListView *ListView1;
	TTabItem *tiMore;
	TImage *imFilm;
	TLabel *laDate;
	TLabel *laGenre;
	TToolBar *ToolBar1;
	TLabel *laName;
	TGridPanelLayout *GridPanelLayout1;
	TLinkListControlToField *LinkListControlToField1;
	TButton *Button2;
	TTabItem *tiRandom;
	TLabel *laDescription;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TStyleBook *StyleBook1;
	TImage *Image1;
	TButton *Button3;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
