//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmAAAClient;
}

TdmAAAClient* Tdm::GetdmAAAClient(void)
{
	if (FdmAAAClient == NULL)
	{
		SQLConnection1->Open();
		FdmAAAClient = new TdmAAAClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmAAAClient;
};

void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
	dm->cdsFilms->Open();
	dm->SQLConnection1->Open();
}
//---------------------------------------------------------------------------


