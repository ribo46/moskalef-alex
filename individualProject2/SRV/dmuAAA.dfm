object dmAAA: TdmAAA
  OldCreateOrder = False
  Height = 371
  Width = 516
  object FilmsConnection: TSQLConnection
    ConnectionName = 'FILMS'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      
        'Database=C:\Users\USER\Desktop\individualProject2\database\VIDEO' +
        '.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = FilmsConnectionAfterConnect
    Connected = True
    Left = 125
    Top = 35
  end
  object taFilms: TSQLDataSet
    Active = True
    CommandText = 'FILMS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = FilmsConnection
    Left = 125
    Top = 83
    object taFilmsID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taFilmsNAME: TWideStringField
      FieldName = 'NAME'
      Size = 400
    end
    object taFilmsIMG: TBlobField
      FieldName = 'IMG'
      Size = 1
    end
    object taFilmsGENRE: TWideStringField
      FieldName = 'GENRE'
      Size = 120
    end
    object taFilmsDATE: TDateField
      FieldName = 'DATE'
    end
    object taFilmsDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 2008
    end
  end
  object dspFilms: TDataSetProvider
    DataSet = taFilms
    Left = 128
    Top = 152
  end
end
