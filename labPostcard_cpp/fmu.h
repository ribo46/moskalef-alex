//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.MediaLibrary.Actions.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.Actions.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Edit.hpp>
#include <System.IOUtils.hpp>
#include <FMX.DialogService.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TGridPanelLayout *gplMenu;
	TButton *buCreate;
	TButton *buLoad;
	TButton *buHelp;
	TButton *buAbout;
	TImage *Image1;
	TLabel *Label6;
	TTabItem *tiPostcard;
	TToolBar *tbPostcard;
	TButton *buBack;
	TButton *buPopup;
	TLabel *laCaption;
	TButton *buAdd;
	TToolBar *tbPostcardBG;
	TLabel *Label1;
	TButton *buPrevBG;
	TButton *buNextBG;
	TButton *buRandomBG;
	TToolBar *tbPostcardIM;
	TLabel *Label2;
	TButton *buPrevIM;
	TButton *buNextIM;
	TButton *buSelectIM;
	TButton *buDel;
	TButton *buRandomIM;
	TLayout *lyPostcard;
	TLayout *ly;
	TGlyph *glyphBG;
	TTabItem *tiIM;
	TToolBar *tbIM;
	TButton *buBackIM;
	TLabel *Label4;
	TButton *buChoiceIM;
	TButton *buIMOut;
	TButton *buIMIn;
	TScrollBox *sbIM;
	TGridLayout *glIM;
	TActionList *al;
	TShowShareSheetAction *acShowShareSheetAction;
	TTabItem *TiOpen;
	TToolBar *tbOpen;
	TButton *buBackOpen;
	TButton *Button3;
	TButton *buOpenPostcard;
	TLabel *Label3;
	TImage *imOpen;
	TSplitter *Splitter1;
	TListView *lvOpen;
	TTabItem *tiSave;
	TToolBar *ToolBar2;
	TButton *buBackSave;
	TLabel *Label5;
	TLabel *Label7;
	TEdit *edFileName;
	TButton *buSaveAs;
	TLayout *Layout1;
	TButton *buSave;
	TButton *buClear;
	void __fastcall SelectionAllMouseDown(TObject *Sender,
		TMouseButton Button, TShiftState Shift, float X, float Y);
	void __fastcall RectangleAllMouseDown(TObject *Sender,
		TMouseButton Button, TShiftState Shift, float X, float Y);
	void __fastcall RectangleAllDblClick(TObject *Sender);
	void __fastcall buCreateClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buPrevBGClick(TObject *Sender);
	void __fastcall buNextBGClick(TObject *Sender);
	void __fastcall buRandomBGClick(TObject *Sender);
	void __fastcall buPrevIMClick(TObject *Sender);
	void __fastcall buNextIMClick(TObject *Sender);
	void __fastcall buSelectIMClick(TObject *Sender);
	void __fastcall buRandomIMClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall lyPostcardMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, float X,
          float Y);
	void __fastcall buBackIMClick(TObject *Sender);
	void __fastcall buIMOutClick(TObject *Sender);
	void __fastcall buIMInClick(TObject *Sender);
	void __fastcall buChoiceIMClick(TObject *Sender);
	void __fastcall glIMResize(TObject *Sender);
	void __fastcall acShowShareSheetActionBeforeExecute(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buLoadClick(TObject *Sender);
	void __fastcall buBackOpenClick(TObject *Sender);
	void __fastcall buOpenPostcardClick(TObject *Sender);
	void __fastcall lvOpenChange(TObject *Sender);
	void __fastcall buBackSaveClick(TObject *Sender);
	void __fastcall buSaveAsClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
	TSelection *FSel;
	TRectangle *FSelIM;
	void ReSetSelection(TObject *Sender);
	void NewImage(); /* overload */
	void NewImage(TImageIndex aIndex); /* overload */
	void ReloadIM();
	void ShowTabIM(TImageIndex aIndex);
	void ReSetIM(TObject *Sender);
	UnicodeString FNamePostcard;
	void ClearPostcard();
	void SavePostcard(UnicodeString aName);
	void OpenPostcard(UnicodeString aName);
	void ReloadOpenList();
	void NewImage(int aIndex, float aX, float aY, float aWidth, float aHeight);

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
const
	UnicodeString cExtSave = ".save";
	UnicodeString cExtPNG = ".png";
    int cSavingWH = 1000;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
