//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Types.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Ani.hpp>
#include <FMX.MaterialSources.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TViewport3D *Viewport3D1;
	TGrid3D *Grid3D1;
	TDummy *Dummy1;
	TPath3D *Path3D1;
	TPath3D *Path3D2;
	TPath3D *Path3D3;
	TPath3D *Path3D4;
	TFloatAnimation *FloatAnimation1;
	TTextureMaterialSource *TextureMaterialSource1;
	TPath3D *Path3D5;
	TTextureMaterialSource *TextureMaterialSource2;
	TPath3D *Path3D6;
	TTextureMaterialSource *TextureMaterialSource3;
	TSphere *Sphere1;
	TTextureMaterialSource *TextureMaterialSource4;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
