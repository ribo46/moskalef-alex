//---------------------------------------------------------------------------

#ifndef dmuClipBoardH
#define dmuClipBoardH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Datasnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
class TdmClipBoard : public TDSServerModule
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TdmClipBoard(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmClipBoard *dmClipBoard;
//---------------------------------------------------------------------------
#endif

